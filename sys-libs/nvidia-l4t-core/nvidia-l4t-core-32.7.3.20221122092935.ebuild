# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="NVIDIA Jetson Linux for Tegra (L4T)"
HOMEPAGE="https://developer.nvidia.com/embedded/jetson-linux"

TARBALL_FILE="Jetson-210_Linux_R32.7.3_aarch64.tbz2"
DEB="nvidia-l4t-core_32.7.3-20221122092935_arm64.deb"

SRC_URI="https://developer.nvidia.com/downloads/remetpack-463r32releasev73t210jetson-210linur3273aarch64tbz2 -> ${TARBALL_FILE}"

KEYWORDS="arm64"

LICENSE="NVIDIA-VARIOUS"
SLOT="0"
RESTRICT=""

L4T_BASEDIR="Linux_for_Tegra"
S="${WORKDIR}/${L4T_BASEDIR}"

BDEPEND="app-arch/dpkg[zstd]"

DOCS=(
	${WORKDIR}/root/usr/share/doc/nvidia-tegra/L4T_End_User_License_Agreement.txt.gz
	${WORKDIR}/root/usr/share/doc/nvidia-tegra/LICENSE.minigbm
	${WORKDIR}/root/usr/share/doc/nvidia-tegra/LICENSE.tegra_sensors
)

src_unpack() {
	emake -f "${FILESDIR}"/GNUmakefile unpack DEB="${DEB}"
	emake -f "${FILESDIR}"/GNUmakefile extract DEB="${DEB}"
}

#src_compile() {
#	emake -f "${FILESDIR}"/GNUmakefile extract DEB="${DEB}"
#}

src_install() {
	default
	emake -f "${FILESDIR}"/GNUmakefile install
}
